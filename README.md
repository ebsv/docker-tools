# Docker Tools

Tools for making life with Docker that much easier

## Installation

`install.sh` installs docker tools to `/usr/local/bin` or `/usr/local/sbin` as appropriate. It also removes the installed files from the other folder, if they are present.

## Tools TOC

1. dbuild
1. dsearch
1. docker-login-gcloud
1. docker-rmi

## 1. dbuild (docker build)

`dbuild` is a command for simplifying a build, push Docker workflow. The main features provided by this command are the ability to specify and run code before the build (to specify or calculate things such as repository name or tags) and after the build (to clean up) - all from the Dockerfile.

_Please be aware that at least one other tool goes by the name `dbuild` (the repository is [here](https://github.com/typesafehub/dbuild)). I don't know anything about it's installation, so use this command at your own risk._

**Example Dockerfile configuration**

```
# This Dockerfile uses dbuild for consistent builds.
# https://bitbucket.org/ebsv/docker-build
#
# start pre-build
#
# # Image repository name
# #
# REPOSITORY="us.gcr.io/ebs-it/myapp"
#
# # Image tag
# #
# TAG="$(date +%s)"
#
# # Perform other pre-build steps here
#
# end pre-build
#
# ================================
#
# start post-build
#
# # Perform any post-build steps here
#
# end post-build
```

For more information, install `dbuild` and run `dbuild --help`

## 2. dsearch

Search our Google Cloud Container Registry for images. Takes all of the same options as `docker search`, but searches our registry.

You can also view containers here: https://console.cloud.google.com/kubernetes/images/list?location=US&project=ebs-it

## 3. docker-login-gcloud

A utility to make it easier to log into our Google Cloud Docker Registry. Recommended usage of Google registry is to use the `gcloud` utility for all tasks related to the registry. However, after logging in with this tool, traditional docker acommands should work.

More information concering the Google Cloud Container Registry can be found on the [Google Cloud Platform documentation site](https://cloud.google.com/container-registry/).

_Related `gcloud` Commands_

```sh
gcloud config list
gcloud config set project ebs-it
docker push us.gcr.io/infrastructure/stackstorm:0.1
gcloud docker push us.gcr.io/infrastructure/stackstorm:0.1
```

## 4. docker-rmi

Remove a docker image. If container dependencies prevent the removal of an image, remove dependencies as well.

_NOTE: This command is potentially destructive. Use only if you intend to destroy all related containers. It will **not** remove container volumes._
