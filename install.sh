#!/bin/bash
#

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

##
# Remove files from /usr/local/bin/ that will be installed into /usr/local/sbin/
##
clean_bin () {
  cd "$DIR/sbin"
  find ./ -type f -exec rm -- '/usr/local/bin/{}' ';' 2> /dev/null
  cd "$DIR"
}

##
# Remove files from /usr/local/sbin/ that will be installed into /usr/local/bin/
##
clean_sbin () {
  cd "$DIR/bin"
  find ./ -type f -exec rm -- '/usr/local/sbin/{}' ';' 2> /dev/null
  cd "$DIR"
}

clean_bin
install -m 755 sbin/* -t /usr/local/sbin

if [ -d bin ]; then
  clean_sbin
  install -m 755 bin/* -t /usr/local/bin
fi

echo "Installation complete. Run \`exec \$SHELL\` to reload your current shell"
